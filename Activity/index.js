let arraySample = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];
console.log("Original Array:\n" + arraySample);

function addArray(addVar) {
	arraySample[arraySample.length] = addVar;
	console.log(arraySample);
}

function returnArrayIndex(indexValue) {
	let returnIndex = arraySample[indexValue];
	return returnIndex;
}

function deleteLastArray() {
	let lastValue = arraySample[arraySample.length-1];
	arraySample.length -= 1;
	return lastValue;
}

function updateArray(value, index) {
	arraySample[index] = value;
}

function deleteAllItemsArray() {
	arraySample.length = arraySample.length - arraySample.length;
}

function checkArray() {
	if(arraySample.length > 0) {
		return false;
	}
	else {
		return true
	}
}

let userInput = prompt("Add a name:");
addArray(userInput);

userInput = prompt("Pick an index number to be returned:")
let itemFound = returnArrayIndex(userInput);
console.log(itemFound);

let deletedArray = deleteLastArray();
console.log(deletedArray);

let valueChosen = prompt("Please choose a value:");
let indexChosen = prompt("Please choose an index:");
updateArray(valueChosen, indexChosen);
console.log(arraySample);

deleteAllItemsArray();
console.log(arraySample);

let arrayValue = checkArray();
console.log(arrayValue);